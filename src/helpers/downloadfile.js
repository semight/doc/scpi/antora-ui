'use strict'

module.exports = (input) => {
  // console.log('downloadfile: ', input)
  switch (input) {
    case 'pxie-smu':
      return '/doc/scpi-site/pxie-smu/1/scpi-programming-guide-pxie-smu.pdf'
    case 'pxie-spgu':
      return '/doc/scpi-site/pxie-spgu/1/scpi-programming-guide-pxie-spgu.pdf'
  }
  return '#'
}
